<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
?>


  <!-- ======= Header ======= -->
  <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>IncuVillage</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="assets/vendor//themify-icons/themify-icons.css">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap-grid.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.7.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <header id="header" class="headers">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
    <a href="index.php" class="logo d-flex align-items-center">
        <img src="assets/img/logo/IncuVillage.svg" alt="" />
      </a>

      <nav id="navbar" class="navbar ms-auto">
        <ul>
        <li><a class="nav-link scrollto" href="index.php#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="index.php#about">Profil</a></li>
          <li><a class="nav-link scrollto" href="index.php#pricing">Belanja</a></li>
          <li><a class="nav-link scrollto" href="index.php#galeri">Galeri</a></li>
          <li><a class="nav-link scrollto" href="index.php#berita">Berita</a></li>
        
          <!-- <li><a href="blog.html">Blog</a></li> -->

     
          <?php if(!isset($_SESSION['pelanggan'])){  ?>
          <li><a class="getstarted scrollto" href="login.php">Masuk</a></li>
          <?php }else{ ?>
         <li><a class="getstarted scrollto" href="logout.php">Logout</a></li>
          <?php } ?>
          <li><div class="site-cart"><?php if(isset($_SESSION['keranjang'])){ ?>
                            <a href="keranjang.php" style="font-size: 21px;" >
                              <i class="icon anm anm-bag-l"></i>
                              <span id="" class="site-header__cart-count"><?= count($_SESSION['keranjang']); ?></span>
                            </a>
                          <?php }else{ ?>
                            <a href="login.php" style="font-size: 21px;" >
                              <i class="icon anm anm-bag-l"></i>
                              <span id="" class="site-header__cart-count">0</span>
                            </a>
                          <?php } ?>
                          </div>
            </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <!-- .navbar -->
    </div>
  </header>

  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  
  <!-- content  -->


  <!--  -->
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="section-headers text-center">
			    <div class="page-titles">
        		<div class="wrapper"><h1 class="page-width">Shopping Cart</h1></div>
      		</div>
		  </div>
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<!-- <div class="alert alert-success text-uppercase" role="alert">
						<i class="icon anm anm-truck-l icon-large"></i> &nbsp;<strong>Congratulations!</strong> You've got free shipping!
					</div> -->
                	<form action="#" method="post" class="cart style2">
                		<table>
                            <thead class="cart__row cart__header">
                                <tr>
                                    <th  class="text-center">No</th>
                                    <th colspan="2" class="text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-right">Total</th>
                                    <th class="action">&nbsp;</th>
                                </tr>
                            </thead>
                    		<tbody id="data_keranjang">
                               
                                <?php 
                                if(!empty($_SESSION['keranjang'])){
                                    $i=1; foreach($_SESSION['keranjang'] as $id_produk => $jumlah){ 
                                   
                                    $getDataProduk = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id_produk'");
                                    $rows = $getDataProduk->fetch_assoc();
                                ?>

                                <tr class="cart__row border-bottom line1 cart-flex border-top">
                                    <td class="text-center small--hide">
                                        <span class=""><?= $i ?></span>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <a href="#"><img class="cart__image" src="assets/img/product/<?= $rows['foto'] ?>" width="150px" height="130px" ></a>
                                    </td>
                                    <td class="cart__meta small--text-left cart-flex-item">
                                        <div class="list-view-item__title">
                                            <a href="#"><?= $rows['nama_produk'] ?></a>
                                        </div>
                                        
                                        <div class="cart__meta-text">
                                           <?= $rows['deskripsi'] ?>
                                        </div>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item">
                                        <span class="money"><?=	"Rp. ".number_format($rows['harga_produk'],0,',','.').",-"; ?></span>
                                    </td>
                                    <td class="cart__update-wrapper cart-flex-item text-right">
                                        <div class="cart__qty text-center">
                                            <div class="qtyField">
                                               
                                                    <input class="cart__qty-input qty" name="jumlah_produk" type="text"  id="qtykeranjang<?= $i ?>" value="<?= $jumlah ?>" pattern="[0-9]*">
                                               
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <?php $harga_produk = $rows['harga_produk'] * $jumlah;  ?>
                                        <div><span class="money"><?=	"Rp. ".number_format($harga_produk,0,',','.').",-"; ?></span></div>
                                    </td>
                                    <td class="text-center small--hide">
                                    <a ref="hapus_keranjang.php?id=<?= $rows['id_produk'] ?>" class="delete"><button type="button" class="btn btn-danger"><i class="bi bi-trash"></i></button></a>
                                    </td>
                                   
                                </tr>
                             
                                <?php $i++; }
                                 }else{ ?>
                                     <tr class="cart__row border-bottom line1 cart-flex border-top"><td colspan="7" class="text-center small--hide mt-3 mb-3"><h2 class="mt-3 mb-3" >Keranjang Kosong</h2></td></tr>
                                 <?php } ?>
                            </tbody>
                    		<tfoot>
                                <tr>
                                  
                                    <td colspan="3" class="text-left"><a href="index.php" class="btn btn-secondary btn--small cart-continue">Continue shopping</a></td>
                                    <td colspan="4" class="text-right">
                                        <?php if(!empty($_SESSION['keranjang'])){ ?>
	                                    <a href="checkout.php" class="btn btn-primary btn--small  small--hide">Checkout</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tfoot>
                    </table> 
                    </form>                   
               	</div>
                
                
                 
            </div>
        </div>
        <section class="shop-services section homes">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-rocket"></i>
							<h4>Free shiping</h4>
							<p>Orders over $100</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-reload"></i>
							<h4>Free Return</h4>
							<p>Within 30 days returns</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-lock"></i>
							<h4>Sucure Payment</h4>
							<p>100% secure payment</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-tag"></i>
							<h4>Best Peice</h4>
							<p>Guaranteed price</p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</section>
    </div>
<?php
  include 'footer.php';
?>
