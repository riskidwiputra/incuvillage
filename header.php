  <!-- ======= Header ======= -->
  <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>IncuVillage</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="assets/vendor//themify-icons/themify-icons.css">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap-grid.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.7.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
    <a href="index.html" class="logo d-flex align-items-center">
        <img src="assets/img/logo/IncuVillage.svg" alt="" />
      </a>

      <nav id="navbar" class="navbar ms-auto">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="#about">Profil</a></li>
          <li><a class="nav-link scrollto" href="#pricing">Belanja</a></li>
          <li><a class="nav-link scrollto" href="#galeri">Galeri</a></li>
          <li><a class="nav-link scrollto" href="#berita">Berita</a></li>
        
          <!-- <li><a href="blog.html">Blog</a></li> -->

     
          <?php if(!isset($_SESSION['pelanggan'])){  ?>
          <li><a class="getstarted scrollto" href="login.php">Masuk</a></li>
          <?php }else{ ?>
         <li><a class="getstarted scrollto" href="logout.php">Logout</a></li>
          <?php } ?>
          <li><div class="sinlge-bar shopping"><?php if(isset($_SESSION['keranjang'])){ ?>
            <a href="keranjang.php" class="single-icon"><i class="ti-bag"></i> <span class="total-count"><?= count($_SESSION['keranjang']); ?></span></a>
                          <?php }else{ ?>
                            <a href="keranjang.php" class="single-icon"><i class="ti-bag"></i> <span class="total-count">0</span></a>
                          <?php } ?>
                          </div>


                         
            </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <!-- .navbar -->
    </div>
  </header>

  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  
  <!-- content  -->


  <!--  -->