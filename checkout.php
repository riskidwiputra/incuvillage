<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
?>
<!-- ======= Header ======= -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>IncuVillage</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="assets/vendor//themify-icons/themify-icons.css">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap-grid.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.7.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <header id="header" class="headers">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
    <a href="index.php" class="logo d-flex align-items-center">
        <img src="assets/img/logo/IncuVillage.svg" alt="" />
      </a>

      <nav id="navbar" class="navbar ms-auto">
        <ul>
        <li><a class="nav-link scrollto" href="index.php#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="index.php#about">Profil</a></li>
          <li><a class="nav-link scrollto" href="index.php#pricing">Belanja</a></li>
          <li><a class="nav-link scrollto" href="index.php#galeri">Galeri</a></li>
          <li><a class="nav-link scrollto" href="index.php#berita">Berita</a></li>
        
          <!-- <li><a href="blog.html">Blog</a></li> -->

     
          <?php if(!isset($_SESSION['pelanggan'])){  ?>
          <li><a class="getstarted scrollto" href="login.php">Masuk</a></li>
          <?php }else{ ?>
         <li><a class="getstarted scrollto" href="logout.php">Logout</a></li>
          <?php } ?>
         
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <!-- .navbar -->
    </div>
  </header>

  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  
  <!-- content  -->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="section-headers text-center">
            <div class="page-titles">
                <div class="wrapper"><h1 class="page-width">Shopping Cart</h1></div>
            </div>
        </div>
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<!-- <div class="alert alert-success text-uppercase" role="alert">
						<i class="icon anm anm-truck-l icon-large"></i> &nbsp;<strong>Congratulations!</strong> You've got free shipping!
					</div> -->
                	<form action="#" method="post" class="cart style2">
                		<table>
                            <thead class="cart__row cart__header">
                                <tr>
                                    <th  class="text-center">No</th>
                                    <th colspan="2" class="text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-right">Total</th>
                                  
                                </tr>
                            </thead>
                    		<tbody id="data_keranjang">
                                <?php 
                                if(isset($_SESSION['keranjang'])){
                                    $totalHarga = 0;
                                    $i=1; foreach($_SESSION['keranjang'] as $id_produk => $jumlah){ 
                                   
                                    $getDataProduk = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id_produk'");
                                    $rows = $getDataProduk->fetch_assoc();
                                ?>

                                <tr class="cart__row border-bottom line1 cart-flex border-top">
                                    <td class="text-center small--hide">
                                        <span class=""><?= $i ?></span>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <a href="#"><img class="cart__image" src="assets/img/product/<?= $rows['foto'] ?>" width="150px" height="130px" ></a>
                                    </td>
                                    <td class="cart__meta small--text-left cart-flex-item">
                                        <div class="list-view-item__title">
                                            <a href="#"><?= $rows['nama_produk'] ?></a>
                                        </div>
                                        
                                        <div class="cart__meta-text">
                                           <?= $rows['deskripsi'] ?>
                                        </div>
                                    </td>
                                    <td class="cart__price-wrapper cart-flex-item">
                                        <span class="money"><?=	"Rp. ".number_format($rows['harga_produk'],0,',','.').",-"; ?></span>
                                    </td>
                                    <td class="cart__update-wrapper cart-flex-item text-right">
                                        <div class="cart__qty text-center">
                                            <div class="qtyField">
                                             
                                                    <input class="cart__qty-input qty" name="jumlah_produk" type="text"  id="qtykeranjang<?= $i ?>" value="<?= $jumlah ?>" pattern="[0-9]*">
    
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right small--hide cart-price">
                                        <?php $harga_produk = $rows['harga_produk'] * $jumlah;  ?>
                                        <div><span class="money"><?=	"Rp. ".number_format($harga_produk,0,',','.').",-"; ?></span></div>
                                    </td>
                                
                                </tr>
                                <?php $i++; $totalHarga += $harga_produk; }
                                 }else{ ?>
                                     <tr class="cart__row border-bottom line1 cart-flex border-top"><td colspan="7" class="text-center small--hide mt-3 mb-3"><h2 class="mt-3 mb-3" >Keranjang Kosong</h2></td></tr>

                                 <?php } ?>
                            </tbody>
                    		<tfoot>
                                <tr>
                                  
                                    <td colspan="7" class="text-left"><a href="keranjang.php" class="btn btn-secondary btn--large cart-continue">Back</a></td>
                                    
                                </tr>
                            </tfoot>
                    </table> 
                    </form>                   
               	</div>
                <div class="container mt-4">
                    <form action="#" method="post">
                        <div class="row">
                        
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mb-4">
                                <h5>Data Pelanggan</h5>
                                
                                    <div class="form-group">
                                        <label for="address_zip">Nama Pelanggan</label>
                                        <input type="text"  value="<?= $_SESSION['pelanggan']['nama_pelanggan'] ?>" readonly class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="address_zip">No Handphone</label>
                                        <input type="text"  value="<?= $_SESSION['pelanggan']['telepon'] ?>" readonly class="form-control">
                                    </div>
                                    
                                
                            
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mb-4">
                            
                                <h5>Pilih Ongkos Kirim</h5>
                            
                                    <div class="form-group col-md-12">
                                        <label for="address_country">Ongkir</label>
                                        <select id="address_country form-control" class="select_ongkir form-control" name="ongkir" data-default="United States" required>
                                        <option hidden >Pilih Ongkos Kirim</option>
                                        <?php 
                                            $getDataOngkir = $koneksi->query("SELECT * FROM ongkir");
                                            while($rowOnkir = $getDataOngkir->fetch_assoc()){
                                        ?>
                                        <option value="<?= $rowOnkir['id_ongkir']?>" data-id="<?= $rowOnkir['tarif'] ?>" ><?= $rowOnkir['nama_kota'] ?> <?=" - Rp. ".number_format($rowOnkir['tarif'],0,',','.').",-"; ?></option>

                                        <?php } ?>
                                        </select>
                                    </div>
                                 
                                    <div class="form-group col-md-12">
                                       
                                        <div class="mb-3">
                                            <label for="validationTextarea">Masukkan Alamat Pengirim</label>
                                            <textarea class="form-control " name="alamat_pengirim" id="CartSpecialInstructions" placeholder="" ></textarea>
                                            
                                        </div>
                                    </div>
                                    
        
                                
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 cart__footer">
                                <div class="solid-border">	
                                <div class="row border-bottom pb-2">
                                    <span class="col-12 col-sm-6 cart__subtotal-title">Subtotal</span>
                                    <span class="col-12 col-sm-6 text-right"><span class="money"><?=	"Rp. ".number_format($totalHarga,0,',','.').",-"; ?></span></span>
                                </div>
                                <div class="row border-bottom pb-2 pt-2">
                                    <span class="col-12 col-sm-6 cart__subtotal-title">Ongkir</span>
                                    <span class="col-12 col-sm-6 text-right" id="ongkir"></span>
                                </div>
                                <div class="row border-bottom pb-2 pt-2">
                                    <span class="col-12 col-sm-6 cart__subtotal-title">Shipping</span>
                                    <span class="col-12 col-sm-6 text-right">Free shipping</span>
                                </div>
                                <div class="row border-bottom pb-2 pt-2">
                                    <span class="col-12 col-sm-6 cart__subtotal-title"><strong>Grand Total</strong></span>
                                    <span class="col-12 col-sm-6 cart__subtotal-title cart__subtotal text-right" style="font-size: 18px;"><strong><span class="money" id="grand_total"></span></strong></span>
                                </div>
                                <div class="cart__shipping">Shipping &amp; taxes calculated at checkout</div>
                                <p class="cart_tearm">
                                    <label>
                                    <input type="checkbox"  class="checkbox" value="tearm" required="">
                                    I agree with the terms and conditions
                                    </label>
                                </p>
                            
                                <button  name="checkout" id="cartCheckout" class="btn btn--small-wide checkout btn-success">Proceed To Checkout</button>
                                <div class="paymnet-img"><img src="assets/img/payment-img.jpg" alt="Payment"></div>
                                <p><a href="#;">Checkout with Multiple Addresses</a></p>
                                </div>
            
                            </div>
                    
                        </div>
                
                    </form>
                    <?php if (isset($_POST['checkout'])){
                    $id_pelanggan = $_SESSION['pelanggan']['id_pelanggan'];
                    $id_ongkir  = $_POST['ongkir'];
                    $alamat     = $_POST['alamat_pengirim'];
                    $tanggal_pembelian =  date("Y-m-d H:i:s");

                    $getDataOngkirs = $koneksi->query("SELECT * FROM ongkir WHERE id_ongkir = '$id_ongkir'");
                    $ongkir         = $getDataOngkirs->fetch_assoc();
                    $totalkeseluruhan   = $ongkir['tarif'] + $totalHarga;
                    $tarif              = $ongkir['tarif'];
                    $nama_kota          = $ongkir['nama_kota'];
                 // var_dump($totalkeseluruhan); 
                    $koneksi->query("INSERT INTO pembelian (id_pelanggan,tanggal_pembelian,total_pembelian,id_ongkir,nama_kota,tarif,alamat_pengiriman) VALUES ('$id_pelanggan','$tanggal_pembelian','$totalkeseluruhan','$id_ongkir','$nama_kota','$tarif','$alamat')");
                    $id_pembelian = $koneksi->insert_id;
                    foreach ($_SESSION['keranjang'] as $id_produk => $jumlah) {
                        $getProduk  = $koneksi->query("SELECT * FROM produk WHERE id_produk='$id_produk'");
                        $dataproduk = $getProduk->fetch_assoc();
                        $nama       = $dataproduk['nama_produk'];
                        $harga      = $dataproduk['harga_produk'];
                        $subharga   = $harga * $jumlah;
                        
                        
                        $updatePorudk = $koneksi->query("UPDATE produk SET terjual='$jumlah' WHERE id_produk = '$id_produk'");
                        $beli = $koneksi->query("INSERT INTO pembelian_produk (id_pembelian,id_produk,jumlah,nama,harga,sub_harga) VALUES ('$id_pembelian','$id_produk','$jumlah','$nama','$harga','$subharga')");
                    }
                    unset($_SESSION['keranjang']);
                    echo "<script>alert('Pembelian Sukses');</script>";
                    echo "<script>location='nota.php?id=$id_pembelian'</script>";
                }
                ?>
                </div>
            </div>
        </div>
        <section class="shop-services section homes">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-rocket"></i>
							<h4>Free shiping</h4>
							<p>Orders over $100</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-reload"></i>
							<h4>Free Return</h4>
							<p>Within 30 days returns</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-lock"></i>
							<h4>Sucure Payment</h4>
							<p>100% secure payment</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-tag"></i>
							<h4>Best Peice</h4>
							<p>Guaranteed price</p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</section>
    </div>
    
    <?php
  include 'footer.php';
?>
<script>

$(document).ready(function(){
	$('.select_ongkir').change(function(){
  		var getData = $(this).find("option:selected").data('id');
            parseInt(getData);
        var ganti   = formatRupiah(String(getData), 'Rp. ');
        var totalHargas = parseInt(<?= $totalHarga ?>);
        let grandTotal  = parseInt(getData)+parseInt(totalHargas);
        let convertgrandTotal  = formatRupiah(String(grandTotal), 'Rp. ');
  		$('#ongkir').html(ganti);
        $('#grand_total').html(convertgrandTotal);
  	});
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
});
</script>