<?php 
session_start();
include 'koneksi.php';
    if(empty($_SESSION['pelanggan']) OR !isset($_SESSION['pelanggan'])){
        echo "<script>alert('Silakan Login terlebih dahulu')</script>";
        echo "<script>location='login.php'</script>";
        header('location:login.php');
    }
    $id_pembelian   = $_GET['id'];
    $ambil = $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pelanggan.id_pelanggan=pelanggan.id_pelanggan WHERE pembelian.id_pembelian='$id_pembelian'");
    $data = $ambil->fetch_assoc();

?>
<!-- ======= Header ======= -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>IncuVillage</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="assets/vendor//themify-icons/themify-icons.css">
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap-grid.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.7.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <header id="header" class="headers">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
    <a href="index.php" class="logo d-flex align-items-center">
        <img src="assets/img/logo/IncuVillage.svg" alt="" />
      </a>

      <nav id="navbar" class="navbar ms-auto">
        <ul>
        <li><a class="nav-link scrollto" href="index.php#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="index.php#about">Profil</a></li>
          <li><a class="nav-link scrollto" href="index.php#pricing">Belanja</a></li>
          <li><a class="nav-link scrollto" href="index.php#galeri">Galeri</a></li>
          <li><a class="nav-link scrollto" href="index.php#berita">Berita</a></li>
        
          <!-- <li><a href="blog.html">Blog</a></li> -->

     
          <?php if(!isset($_SESSION['pelanggan'])){  ?>
          <li><a class="getstarted scrollto" href="login.php">Masuk</a></li>
          <?php }else{ ?>
         <li><a class="getstarted scrollto" href="logout.php">Logout</a></li>
          <?php } ?>
         
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <!-- .navbar -->
    </div>
  </header>

  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  
  <!-- content  -->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="section-headers text-center">
            <div class="page-titles">
                <div class="wrapper"><h1 class="page-width">Nota Pembelian</h1></div>
            </div>
        </div>
        <div class="container mb-4" >
            <div class="row">
                <div class="col-md-3">
                    <div class="alert alert-success">
                        <div class="h3" style="font-weight: bold;">RDP Stores</div>
                        <font style="font-size: 15px">
                        <p style="margin-top: 15px; ">
                        <i class="fas fa-map-marker-alt"></i>
                        Jl. Karya Kasih Gg.kasih dalam no.37, Medan.<br> 
                        <i class="fas fa-address-book"></i>
                        +62 323 321 231 <br>
                        <i class="fas fa-envelope"></i>
                        RDPStores@example.com
                        </p>
                    </font>
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="alert alert-success" style="padding-bottom: 23px;">
                        <div class="h3" style="font-weight: bold;">Pembelian</div>
                        <font style="font-size: 16px">
                        <font style="" >Id pembelian : #<?= $data['id_pembelian']; ?></font>
                       
                            <p >
                            <i class="fas fa-calendar-alt"> <?= $data['tanggal_pembelian']; ?> </i><br>
                            
                            Status : <font style="color: red;"> Dipesan</font>
                            </p>
                        </font>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="alert alert-success" style="padding-bottom: 20px; ">
                        <div class="h3" style="font-weight: bold;">Pelanggan</div>
                        <font style="font-size: 16px">
                        <!-- <p style="margin-top: 25px; "> -->
                        <i class="fas fa-user"></i>
                    <?= $data['nama_pelanggan']; ?><br> 
                        <i class="fas fa-phone-square-alt"></i></i>
                        <?= $data['telepon']?> <br>
                    <i class="fas fa-address-book"></i>
                        <?= $data['email_pelanggan'] ?>
                        </p>
                    </font>
                   
                    </div>
                </div>
                <div class="col-md-3"> 
                    <div class="alert alert-success" style="padding-bottom: 10px;">
                        <div class="h3" style="font-weight: bold;">Pengiriman</div>
                        <font style="font-size: 15px;" >
                        <p style="margin-top: 10px; ">
                       
                        <?= $data['nama_kota'];?><br> 
                        Ongkos Ongkir : <?=	"Rp. ".number_format($data['tarif'],0,',','.').",-"; ?>
                        Alamat  : <?= $data['alamat_pengiriman'] ?>
                    </font>
                  
                    </div>
                </div>
                <div class="col-12">
                    <div class="table-responsive">
                        
                         <h2>Detail Produk</h2>
                        <table class="table table-striped">
                            <thead>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Gambar Produk</th>
                           
                                <th>Harga</th>
                                <th>Jumlah</th>
                            
                                <th>Sub Total</th>
                            </thead>
                            <tbody>
                            <?php 
                            $no=1;
                                $getProduk = $koneksi->query("SELECT * FROM pembelian_produk LEFT JOIN produk ON produk.id_produk=pembelian_produk.id_produk WHERE id_pembelian='$id_pembelian' ");
                                while ($rows = $getProduk->fetch_assoc()) {
                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $rows['nama'] ?></td>
                                    <td><img src="assets/img/product/<?= $rows['foto'] ?>" alt="" style="width: 150px; height:150px"></td>

                                    <td><?=	"Rp. ".number_format($rows['harga'],0,',','.').",-"; ?></td>
                                    <td><?= $rows['jumlah'] ?></td>
                                  
                                    <td><?=	"Rp. ".number_format($rows['sub_harga'],0,',','.').",-"; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p style="font-size: 17px; font-family: arial, sent-serif;">
                            SIlakan melakukan Pembayaran <font style="color: red; font-weight: bold;"> <?=	"Rp. ".number_format($data['total_pembelian'],0,',','.').",-"; ?></font> ke <br><font style="font-weight: bold;">  <strong>Bank Mandiri <br></strong></font>Rek : 173-000321412414 <br> AN : Blalbala  <br>
                            <br>
                            Dan Silakan konfirmasi pembayaran setelah anda membayar tagihan ini <br>
                            Produk dikirim setelah melakukan pembayaran <br>
                            Silakan Hubungi<br>
                            No : <font style="color: red; font-weight: bold;">0813-1242-6234</font> <br>
                            Wa : <font style="color: red; font-weight: bold;">0852-3215-2314</font>


                        </p>
                    </div>
                </div>
            </div>
        </div>
        <section class="shop-services section homes">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-rocket"></i>
							<h4>Free shiping</h4>
							<p>Orders over $100</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-reload"></i>
							<h4>Free Return</h4>
							<p>Within 30 days returns</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-lock"></i>
							<h4>Sucure Payment</h4>
							<p>100% secure payment</p>
						</div>
						<!-- End Single Service -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Start Single Service -->
						<div class="single-service">
							<i class="ti-tag"></i>
							<h4>Best Peice</h4>
							<p>Guaranteed price</p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</section>
    </div>
<?php
  include 'footer.php';
?>